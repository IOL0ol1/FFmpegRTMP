﻿using EmguFFmpeg;


using System.Windows;
using System.Windows.Controls.Primitives;

namespace RTMPClient
{
    using FFmpeg.AutoGen;
    using NAudio.Wave;
    using Properties;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private WriteableBitmap writeableBitmap;
        private ManualResetEventSlim manualResetEventSlim = new ManualResetEventSlim(true);

        public MainWindow()
        {
            InitializeComponent();
            EmguFFmpeg.FFmpeg.RegisterBinaries("./FFmpeg");
        }


        private void ToggleButton_Click(object sender, RoutedEventArgs e)
        {
            Settings.Default.Save();
            bool? isChecked = (sender as ToggleButton).IsChecked;
            if (isChecked.HasValue && isChecked.Value)
            {
                manualResetEventSlim.Reset();
                Task.Run(RtmpFunc);
            }
            else
            {
                manualResetEventSlim.Set();
            }
        }

        private void RtmpFunc()
        {
            using (MediaReader reader = new MediaReader(Settings.Default.RemoteURL))
            {
                int videoIndex = reader.Where(_ => _.Codec.Type == AVMediaType.AVMEDIA_TYPE_VIDEO).First().Index;
                int audioIndex = reader.Where(_ => _.Codec.Type == AVMediaType.AVMEDIA_TYPE_AUDIO).First().Index;

                int height = reader[videoIndex].Codec.AVCodecContext.height;
                int width = reader[videoIndex].Codec.AVCodecContext.width;

                PixelConverter pixelConverter = new PixelConverter(AVPixelFormat.AV_PIX_FMT_BGR24, width, height);

                ImageDisplay.Dispatcher.Invoke(() =>
                {
                    writeableBitmap = new WriteableBitmap(width, height, 96.0, 96.0, PixelFormats.Bgr24, null);
                    ImageDisplay.Source = writeableBitmap;
                });

                foreach (var srcPacket in reader.ReadPacket())
                {
                    foreach (var srcFrame in reader[srcPacket.StreamIndex].ReadFrame(srcPacket))
                    {
                        if (srcPacket.StreamIndex == videoIndex)
                        {
                            var dstFrame = pixelConverter.ConvertFrame(srcFrame);
                            ImageDisplay.Dispatcher.Invoke(() =>
                            {
                                writeableBitmap.WritePixels(new Int32Rect(0, 0, writeableBitmap.PixelWidth, writeableBitmap.PixelHeight), dstFrame.Data[0], dstFrame.Linesize[0] * writeableBitmap.PixelHeight, dstFrame.Linesize[0]);
                            });
                        }
                        else if (srcPacket.StreamIndex == audioIndex)
                        {

                        }
                    }
                    if (manualResetEventSlim.IsSet)
                        break;
                }
            }
        }

    }
}
