﻿using EmguFFmpeg;
using FFmpeg.AutoGen;
using Microsoft.Win32;

using RTMPServer.Properties;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace RTMPServer
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private WriteableBitmap writeableBitmap;
        private ManualResetEventSlim manualResetEventSlim = new ManualResetEventSlim(true);
        public MainWindow()
        {
            InitializeComponent();
            EmguFFmpeg.FFmpeg.RegisterBinaries("./FFmpeg");
        }

        private void ToggleButton_Click(object sender, RoutedEventArgs e)
        {
            Settings.Default.Save();
            bool? isChecked = (sender as ToggleButton).IsChecked;
            if (isChecked.HasValue && isChecked.Value)
            {
                manualResetEventSlim.Reset();
                Task.Run(RtmpFunc);
            }
            else
            {
                manualResetEventSlim.Set();
            }
        }


        private void RtmpFunc()
        {
            using (MediaReader reader = new MediaReader(Settings.Default.LocalURL))
            using (MediaWriter writer = new MediaWriter(Settings.Default.RemoteURL))
            {

                int videoIndex = reader.Where(_ => _.Codec.Type == AVMediaType.AVMEDIA_TYPE_VIDEO).First().Index;
                int audioIndex = reader.Where(_ => _.Codec.Type == AVMediaType.AVMEDIA_TYPE_AUDIO).First().Index;

                int height = reader[videoIndex].Codec.AVCodecContext.height;
                int width = reader[videoIndex].Codec.AVCodecContext.width;
                int fps = reader[videoIndex].Stream.r_frame_rate.num;

                ImageDisplay.Dispatcher.Invoke(() =>
                {
                    writeableBitmap = new WriteableBitmap(width, height, 96.0, 96.0, PixelFormats.Bgr24, null);
                    ImageDisplay.Source = writeableBitmap;
                });

                int channels = reader[audioIndex].Codec.AVCodecContext.channels;
                int samplerate = reader[audioIndex].Codec.AVCodecContext.sample_rate;

                // use livego supported format
                writer.AddStream(MediaEncode.CreateVideoEncode(AVCodecID.AV_CODEC_ID_H264, writer.Format.Flags, width, height, fps));
                writer.AddStream(MediaEncode.CreateAudioEncode(AVCodecID.AV_CODEC_ID_AAC, writer.Format.Flags, channels, samplerate));
                writer.Initialize();

                PixelConverter pixelConverterDisplay = new PixelConverter(AVPixelFormat.AV_PIX_FMT_BGR24, width, height);
                PixelConverter pixelConverter = new PixelConverter(writer[0].Codec);
                SampleConverter sampleConverter = new SampleConverter(writer[1].Codec);

                long videoPts = 0;
                long audioPts = 0;
                foreach (var srcPacket in reader.ReadPacket())
                {
                    foreach (var srcFrame in reader[srcPacket.StreamIndex].ReadFrame(srcPacket))
                    {
                        if (srcPacket.StreamIndex == videoIndex) // 视频
                        {
                            var displayFrame = pixelConverterDisplay.ConvertFrame(srcFrame);
                            ImageDisplay.Dispatcher.Invoke(() =>
                            {
                                writeableBitmap.WritePixels(new Int32Rect(0, 0, writeableBitmap.PixelWidth, writeableBitmap.PixelHeight), displayFrame.Data[0], displayFrame.Linesize[0] * writeableBitmap.PixelHeight, displayFrame.Linesize[0]);
                            });
                            foreach (var dstFrame in pixelConverter.Convert(srcFrame))
                            {
                                dstFrame.Pts = videoPts++;
                                foreach (var dstPacket in writer[0].WriteFrame(dstFrame))
                                {
                                    writer.WritePacket(dstPacket);
                                }
                            }
                        }
                        if (srcPacket.StreamIndex == audioIndex) // 音频
                        {
                            foreach (var dstFrame in sampleConverter.Convert(srcFrame))
                            {
                                audioPts += dstFrame.NbSamples;
                                dstFrame.Pts = audioPts;
                                foreach (var dstPacket in writer[1].WriteFrame(dstFrame))
                                {
                                    writer.WritePacket(dstPacket);
                                }
                            }
                        }
                    }
                    if (manualResetEventSlim.IsSet) // 退出
                        break;
                }
                writer.FlushMuxer();
            }
        }









        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                Settings.Default.LocalURL = openFileDialog.FileName;
                Settings.Default.Save();
            }
        }
    }
}
